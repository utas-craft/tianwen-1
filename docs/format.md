# Data format

## 1 SHAO DF

File format: dat

Chan 1: Tianwen–1
Chan 2: MEX

Column 1: UTC time ****(year)***(doyday)**(hour)**(min)**.***(sec)
Column 2: A reference frequency (Hz)
Column 3: The measured Doppler frequency (Hz)
Column 4: The formal error (Hz)
Column 5: signal-to-noise (dBHz)

## 2 PRIDE DF

File format: txt

4 header llines

Column 1: MJD 
Column 2: Time (UTC) in s
Column 3: SNR
Column 4: Spectral max
Column 5: Freq detection [Hz]
Column 6: Doppler Noise [Hz]

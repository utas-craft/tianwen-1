# Solar Corona experiment with Tianwen-1 :rocket:

Coordinated observations with Shanghai observatory for solar corona experiments

You can access these information files:

- [Transmission times](docs/schedule.md)
- [Transfer status](docs/transfers.md)

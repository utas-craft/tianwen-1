[[_TOC_]]

# Summary of the sessions transferred

## 1. Summary
We started a joint effort to observe in parallel Mars Express and Tianwen-1 sessions. The aim is to study the solar corona at low solar elongations. The solar occultation will occur September 2021. The sessions are carried out with the telescopes of UTAS and VLBI China.

## 2. Short list of tasks to do

- [ ] 

## 3. Current status

|    Epoch   | Station | Required | Transfer way    |      Status        |
|------------|---------|----------|-----------------|--------------------|
| 16.07.2021 | Hb/Ke   |   Yes    | UTAS -> SHAO    | :white_check_mark: |
| 23.07.2021 |   Sh    |   Yes    | SHAO -> UTAS    | :white_check_mark  |
| 24.07.2021 |   Hb    |   Yes    | UTAS -> SHAO    | :white_check_mark: |
| 16.08.2021 | Hb/Yg   |   Yes    | UTAS -> SHAO    | :white_check_mark  |
| 27.08.2021 |   Hh    |   Yes    | SHAO -> UTAS    | :white_check_mark  |
| 28.08.2021 |  Ht/Wz  |   Yes    | SHAO -> UTAS    | :white_check_mark  |
| 29.08.2021 |  Sh/Wz  |   Yes    | SHAO -> UTAS    | :white_check_mark  |

